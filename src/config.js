'use strict'

const config = {}

config.fastify = {
  address: '0.0.0.0',
  port: 8443
}

config.logger = {
  pretty: true,
  level: 'trace'
}

config.neo4j = {
  host: 'bolt://0.0.0.0:7687',
  username: 'neo4j',
  password: 'Kv3spana!'
}

module.exports = config