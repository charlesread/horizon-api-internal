'use strict'

const EventEmitter = require('events')

const config = require('~/config')
const log = require('~/lib/logger')()

const neo4j = require('neo4j-driver').v1

const implementation = {}

implementation.driver = neo4j.driver(
  config.neo4j.host,
  neo4j.auth.basic(config.neo4j.username, config.neo4j.password)
)

// implementation.driver = neo4j.driver(config.neo4j.host)

function cleanup() {
  try {
    implementation.driver.close()
      .catch(function (err) {
        // so what
      })
  } catch (err) {
    // so what
  }
}

function closeSession(session) {
  try {
    session.close()
      .catch(function (err) {
        // so what
      })
  } catch (err) {
    // so whats
  }
}

process.on('SIGINT', cleanup)
process.on('SIGTERM', cleanup)

implementation.getPathsStream = function (uriFrom, uriTo, hopMin = 1, hopMax = 3) {
  return new Promise(function (resolve, reject) {
    const ee = new EventEmitter()
    let session
    try {
      session = implementation.driver.session()
      // session = implementation.driver.session({defaultAccessMode: neo4j.session.READ})
      session
        .run(
          'match r = (f:Page {uri: $uriFrom}) - [l:LINKS_TO *$hopMin..$hopMax] -> (t:Page {uri: $uriTo}) return r',
          {
            uriFrom,
            uriTo,
            hopMin,
            hopMax
          }
        )
        .subscribe({
          onKeys: function (keys) {
            ee.emit('keys', keys)
          },
          onNext: function (record) {
            ee.emit('record', record)
          }, // called for each record returned from the graph query #streaming
          onCompleted: () => {
            closeSession(session)
            ee.emit('end')
          },
          onError: function (err) {
            ee.emit('error', err)
          }
        })
      return resolve(ee)
    } catch (err) {
      reject(err)
    }
  })
}

implementation.getPathsPromise = async function (uriFrom, uriTo, hopMin = 1, hopMax = 3) {
  let statement = `match r = (f:Page {uri: $uriFrom}) - [l:LINKS_TO *${hopMin}..${hopMax}] -> (t:Page {uri: $uriTo}) return r limit 100`
  log.trace(`invoking neo.getPathsPromise(${uriFrom}, ${uriTo}, ${hopMin}, ${hopMax}), statement: ${statement}`)
  let session
  try {
    session = implementation.driver.session(neo4j.session.READ)
    // session = implementation.driver.session({defaultAccessMode: neo4j.session.READ})
    let result = await session.run(
      statement,
      {
        uriFrom,
        uriTo
      }
    )
    closeSession(session)
    let paths = []
    for (let record of result.records) {
      let path = []
      for (let pathSegment of record['_fields'][0]['segments']) {
        let pathSegmentObject = {}
        pathSegmentObject.start = pathSegment.start.properties.uri
        pathSegmentObject.end = pathSegment.end.properties.uri
        pathSegmentObject.relationship = {
          properties: pathSegment.relationship.properties,
          type: pathSegment.relationship.type
        }
        path.push(pathSegmentObject)
      }
      paths.push(path)
    }
    return {
      marshalled: paths,
      raw: result
    }
  } catch (err) {
    log.error(`error in neo.getPathsPromise(): ${err.message}`)
    log.debug(err.stack)
    closeSession(session)
    throw err
  }
}

module.exports = implementation