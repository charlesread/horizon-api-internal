'use strict'

const log = require('~/lib/logger')()
const neo = require('~/lib/neo')

module.exports = function (fastify, opts, next) {

  fastify.get(
    '/',
    async function (req, reply) {
      reply.view('pages/slash/index', {})
    })

  next()

}