'use strict'

const log = require('~/lib/logger')()
const neo = require('~/lib/neo')

module.exports = function (fastify, opts, next) {

  fastify.get(
    '/path',
    {
      schema: {
        querystring: {
          type: 'object',
          properties: {
            to: {
              type: 'string'
            },
            from: {
              type: 'string'
            },
            hops: {
              type: 'integer',
              minimum: 1,
              maximum: 6
            },
            hopsMin: {
              type: 'integer',
              minimum: 1,
              maximum: 6
            },
            hopsMax: {
              type: 'integer',
              minimum: 1,
              maximum: 6
            }
          },
          required: ['to', 'from']
        }
      }
    },

    async function (req, reply) {

      // const emitter = await neo.getPaths('/wiki/Philosophy', '/wiki/Existence')
      //
      // emitter.on('record', function (record) {
      //   console.log(record)
      // })
      //
      // emitter.on('error', function (err) {
      //   log.error(err.message)
      // })
      //
      // reply.send()

      let payload = {}

      try {
        const from = req.query.from
        const to = req.query.to
        let hopsMin = req.query.hopsMin
        let hopsMax = req.query.hopsMax
        const hops = req.query.hops
        if (hops) {
          hopsMin = hops
          hopsMax = hops
        }
        payload = await neo.getPathsPromise(from, to, hopsMin, hopsMax)
      } catch (err) {
        log.error(err.message)
        log.debug(err.stack)
        payload = err
      } finally {
        reply.send(payload)
      }

    }
  )

  next()

}